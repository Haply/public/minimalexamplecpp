# Example of using the Haply HardwareAPI with C++

This repository contains a suite of simple examples for using Haply's Inverse3
and handles using the C++ Hardware API.

## Build

Builds are managed through [cmake](https://cmake.org) and can be invoked from
the command line using the following commands:

```shell
$ cmake . -B ./build -DCMAKE_BUILD_TYPE=Release
$ cmake --build ./build
$ cmake --install ./build
```

A [`CMakeSettings.json`](./CMakeSettings.json) file is also provided to simplify
builds with [Visual Studio](https://visualstudio.microsoft.com/). You'll need to
manually install the exectuables once the build is complete using the menu `Build >
Install HardwareAPI.Examples`.

Note that the build will automatically download the latest version of the C++
API and extract it into the `HardwareAPI` folder. This folder will include both
the static library for the API along with the headers.

## Examples

The output of the build and installation process will be placed in a `bin`
folder located at the root of the repository and will contain an executable for each
of the examples that are described below:

- [`00-auto-detect`](./00-auto-detect) Uses the API's serial ports auto-detection
  feature to find all available Inverse3 and Handle devices along with their
  port. This utility is useful for running the other demos which will require
  that ports be provided as command line arguments.
  
- [`01-print-inverse3`](./01-print-inverse3) Connects to an Inverse3 device and
  outputs its position and velocity values as you move its end-effector.

- [`02-print-handle`](./02-print-handle) Connects to a Handle device and outputs
  its orientation and state values as you move it around.

- [`03-hello-wall`](./03-hello-wall) Simple haptics demo where you can push
  against an infinite plane.
  
- [`04-combined`](./04-combined) More complex demo that combines both an
  Inverse3 and a handle.
