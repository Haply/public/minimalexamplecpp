# Kinematic Measurement Inverse3

This folder contains an example that show how to do a high precision kinematic measurement with the inverse3 on the computer.

The `05-kinematic-measurement` executable takes the COM ports of an Inverse3 as an argument. As an example:

```shell
$ ./bin/05-kinematic-measurement
```

The example will use the first inverse3 found.
