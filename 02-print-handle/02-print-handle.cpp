/* -*- mode: c++ -*-
 * Copyright 2022 Haply Robotics Inc. All rights reserved.
 *
 * Simple example of how to read the orientation of the Quill Handle which we
 * will print to stdout.
 */

#include <cassert>
#include <cstdio>

// The primary include file for the Haply C++ API.
#include "HardwareAPI.h"

// Used to reduce verbosity within the examples.
namespace API = Haply::HardwareAPI;

// To make use of a handle we must first specialize the API's
// `API::Devices::Handle` class which contains overridable methods to invoked
// whenever a response is received from the handle.
struct QuillHandle : public API::Devices::Handle {

    // Simple constructor that fronts the Handle constructor.
    QuillHandle(API::IO::SerialStream *stream) : Handle(stream) {}

  protected:
    // This override will be called right after the device is awakened via
    // `SendDeviceWakeup` and its parameters represent the general device
    // information.
    void OnReceiveHandleInfo(uint8_t, uint16_t device_id,
                             uint8_t device_model_number,
                             uint8_t hardware_version,
                             uint8_t firmware_version) override {
        std::fprintf(
            stdout,
            "info: id=%04x, model=%u, version={hardware:%u, firmware:%u}\n",
            device_id, device_model_number, hardware_version, firmware_version);
    }

    // This override is invoked whenever a new state is received from the device
    // as a response to `SendHandleState` or `RequestStatus`. The useful
    // parameters of the method are:
    //
    // - `quaternion`: orientation of the handle in an RIJK ordered
    //   quaternion. The pointer should be read as an array of 4 floats.
    //
    // - `attached`: represents whether the handle is attached or not to an
    //   Inverse3 device where 1 indicates that it's attached and 0 indicates
    //   that it's not attached. Note that, in a scenario where there are
    //   multiple Inverse3 devices in use, it's currently not possible to
    //   determine which Inverse3 a handle is attached to.
    //
    // - `user_data_length`: number of additional extra handle-specific bytes of
    //   data that can be read in the `user_data` array. For the Quill handle,
    //   there will always be 4 bytes that can be read.
    //
    // - `user_data[0..2]`: for the Quill handle, the first 3 bytes are used to
    //   represent whether one of the buttons on the Quill is pressed where 0
    //   means that it's not pressed and 1 represents that it's pressed.
    //
    // - `user_data[3]`: for the Quill handle, represents the current charge on
    //   the handle's battery where a value of 100 means that it's fully charged
    //   and a value of 0 means that the battery has run out.
    void OnReceiveHandleStatusMessage(uint16_t, float *quaternion, uint8_t,
                                      uint8_t attached,
                                      [[maybe_unused]] uint8_t user_data_length,
                                      uint8_t *user_data) override {
        assert(user_data_length == 4);
        uint8_t battery = user_data[3];

        std::fprintf(stdout,
                     "\r"
                     "attached=%u, battery=%03u, buttons=[ %u, %u, %u ], "
                     "quaternion=[ % 0.3f % 0.3f % 0.3f % 0.3f ]",
                     attached, battery, user_data[0], user_data[1],
                     user_data[2], quaternion[0], quaternion[1], quaternion[2],
                     quaternion[3]);
    }
};

int main(int argc, const char *argv[]) {
    if (argc < 2) {
        std::fprintf(stderr, "usage: 02-print-handle [com-port]\n");
        return 1;
    }

    // The `API::IO::SerialStream` object encapsulates the serial COM port that
    // will be used to communicate with a device. The argument given to its
    // constructor should be a COM port (e.g. COM6).
    API::IO::SerialStream stream{argv[1]};

    // Using the `API::IO::SerialStream` object we can initialize the
    // `QuillHandle` object defined earlier in this file which will encapsulates
    // all the logic needed to interact with a Quill handle.
    QuillHandle device{&stream};

    // To start using the device, we first need to wake it up using the
    // `SendDeviceWakeup` method followed by a call to `Receive` to process the
    // response from the device. Once awake, the LED colour on the device should
    // change to indicate that it's ready to receive commands.
    device.SendDeviceWakeup();

    // Each `SendXxx` method call on a handle object should be paired with a
    // `Receive` method call to process the response from the device. In this
    // case, the response for the `SendDeviceWakeup` function will trigger the
    // `OnReceiveHandleInfo` override to be called in our `QuillHandle` class.
    (void)device.Receive();

    while (true) {

        // Given that the API works synchronously, we need to call
        // `RequestStatus` each time we want to query the latest handle state.
        device.RequestStatus();

        // The response for the `RequestStatus` method will trigger the
        // `OnReceiveHandleStatusMessage` override to be called in our
        // `QuillHandle` class.
        (void)device.Receive();
    }

    return 0;
}
