# Simple State Printer for Handle

This folder contains an example of how to connect to a Haply handle, wake it up,
receive its general device information, and print the device's orientation and
status as you move it. We will focus ourselves on the Quill handle provided by
Haply. Custom handles are handled in a very similar fashion.

The `02-print-handle` executable takes the COM port of a handle as an
argument. As an example:

```shell
$ ./bin/02-print-handle COM16
```

To quickly find the COM port associated with your plugged in handle, you can use
the [`00-auto-detect`](../00-auto-detect) demo.
