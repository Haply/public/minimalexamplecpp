# Copyright 2022 Haply Robotics Inc. All rights reserved.
project(04-combined LANGUAGES CXX)
add_executable(${PROJECT_NAME} ${PROJECT_NAME}.cpp)
install(TARGETS ${PROJECT_NAME})
set(SOURCE_FILES ${SOURCE_FILES} ${PROJECT_SOURCE_DIR}/${PROJECT_NAME}.cpp PARENT_SCOPE)
