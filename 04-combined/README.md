# Combined Inverse3 and Handle Example

This folder contains an example that uses a the orientation of a handle to move
the end-effector of an Inverse3 using position control. It combines the API for
the Inverse3 and the handles and shows how to use threads to manage the
difference in polling frequencies between the two.

The `04-combined` executable takes the COM ports of an Inverse3 and handle
devices as an argument. As an example:

```shell
$ ./bin/04-combined COM6 COM16
```

Where `COM6` is the COM port for an Inverse3 and `COM16` the COM port for a
handle. To quickly find the COM port associated with your plugged devices, you
can use the [`00-auto-detect`](../00-auto-detect) demo.
