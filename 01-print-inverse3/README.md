# Simple State Printer for Inverse3

This folder contains a simple example of how to connect to an Inverse3 device,
wake it up, receive its general device information, and print the device's end
effector position and velocity as you move it. In essence these are the basics
for using the Inverse3 as a 3D mouse.

The `01-print-inverse3` executable takes the COM port of an Inverse3 as an
argument. As an example:

```shell
$ ./bin/01-print-inverse3 COM6
```

To quickly find the COM port associated with your plugged in Inverse3, you can
use the [`00-auto-detect`](../00-auto-detect) demo.
