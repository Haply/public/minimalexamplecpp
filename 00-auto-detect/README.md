# Auto-Detection Example

This folder contains an example of how to use the C++ API's auto-detection
feature to list all the currently connected Inverse3 and Handles along with
their associated serial port. The output of this demo will be useful for
providing the command line arguments of the other demos.

No arguments need to be provided to the executable.
