# Simple Haptic Feedback Example

This folder contains an example for a basic haptic feedback loop using the
Inverse3 device. It sets up an abitrary wall (floor) in the device's workspace
that you can push against.

The `03-hello-wall` executable takes the COM port of an Inverse3 device as an
argument. As an example:

```shell
$ ./bin/03-hello-wall COM16
```

To quickly find the COM port associated with your plugged in handle, you can use
the [`00-auto-detect`](../00-auto-detect) demo.
